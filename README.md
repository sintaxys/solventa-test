Hola, gracias por brindarme esta oportunidad, mi nombre es Daniel Felipe Delgado y esta es mi prueba ténica

A continuación dejaré los pasos para poder ejecutar la aplicación:

Pre-requisitos:
  - Node 12.18.0

Instalación:
  1. en la carpeta root del proyecto, ejecutar el comando "npm install" para instalar las dependencias
  2. ejecutar el comando "npm run dev" para levantar el servidor de desarrollo que por defecto se encuentra en http://localhost:3000

Stack usado:
  - Se hace uso de Nuxt, que es una libreria que permite realizar Server Side Rendering con VueJS y además permite integrar de forma muy facil demás librerias del ecosistema de VueJS
  - Se hace uso de la libreria Vuetify como UI Components con diseño de Material Design, para realizar la maquetación de los componentes y hacer el diseño reponsivo usando la grid integrada en dicha libreria
  - Prettier y ESLint para calidad de código

Muchas gracias!